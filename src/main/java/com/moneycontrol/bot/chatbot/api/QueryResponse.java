package com.moneycontrol.bot.chatbot.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.net.URL;
import java.util.Optional;

/**
 * @author by vivekkothari on 16/12/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryResponse {

    private final Object conversationResponse;
    private final Optional<String> maybePassage;
    private final Optional<URL> maybeArticleUrl;

    public QueryResponse(Object conversationResponse, Optional<String> maybePassage, Optional<URL> maybeArticleUrl) {
        this.conversationResponse = conversationResponse;
        this.maybePassage = maybePassage;
        this.maybeArticleUrl = maybeArticleUrl;
    }

    public Optional<String> getMaybePassage() {
        return maybePassage;
    }

    public Optional<URL> getMaybeArticleUrl() {
        return maybeArticleUrl;
    }

    public Object getConversationResponse() {
        return conversationResponse;
    }
}
