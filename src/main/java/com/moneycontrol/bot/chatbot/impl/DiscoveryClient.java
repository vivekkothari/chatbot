package com.moneycontrol.bot.chatbot.impl;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.ibm.watson.developer_cloud.discovery.v1.Discovery;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.moneycontrol.bot.chatbot.api.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Optional;

/**
 * @author by vivekkothari on 16/12/17.
 */
@Component
public class DiscoveryClient {

    @Autowired
    private Discovery discovery;

    @Value("${watson.discovery.environment.id}")
    private String environmentId;

    @Value("${watson.discovery.collection.id}")
    private String collectionId;

    @Value("${watson.discovery.username}")
    private String username;

    @Value("${watson.discovery.password}")
    private String password;

    private static final Logger log = LoggerFactory.getLogger(DiscoveryClient.class);

    public DiscoveryClient() {
        final com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        Unirest.setObjectMapper(new ObjectMapper() {
            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return mapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return mapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    public QueryResponse discover(final String text) throws Exception {
        String endPoint = String.format("%s/v1/environments/%s/collections/%s/query", discovery.getEndPoint(), environmentId, collectionId);
        String url = String.format("%s?version=2017-11-07&deduplicate=false&highlight=true&passages=true&passages.count=5&natural_language_query=%s", endPoint, URLEncoder.encode(text, "UTF-8"));
        HttpResponse<Map> response = Unirest.get(url)
                .basicAuth(username, password)
                .asObject(Map.class);
        log.info("Response from discovery:");
        log.info(response.getBody().toString());
        return new QueryResponse(response.getBody(), Optional.empty(), Optional.empty());
    }

}
