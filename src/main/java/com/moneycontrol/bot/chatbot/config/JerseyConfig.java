package com.moneycontrol.bot.chatbot.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletProperties;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;

/**
 * @author by vivekkothari on 16/12/17.
 */
@Configuration
@ApplicationPath("/chatbot")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        packages("com.moneycontrol.bot.chatbot");
        property(ServletProperties.FILTER_FORWARD_ON_404, true);
    }

}
