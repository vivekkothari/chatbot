# README #

### What is this repository for? ###

* This is a chatbot, which answers your questions on Indian Budget.
* 1.0

### How do I get set up? ###

* Prerequisites: You need Java 8 installed. Chrome browser.
* IBM Watson: Converstion and Discovery services.
* Run the com.moneycontrol.bot.chatbot.ChatbotApplication class.


### Who do I talk to? ###

* Please contact: Antriksh, Rachit and Vivek for more details.
